// Creating the Schema, model and export file

const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({

	// field: data_type
	name: String,
	// fieldL { options }
	status: {
		type: String,
		default: "pending"
	}
});

// ("Task" => collection name "tasks")
module.exports = mongoose.model("Task", taskSchema)