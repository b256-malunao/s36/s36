// Contains all the URI endpoints for our application

const express = require("express");
// Creates a Router instance that functions as a middleware and routing system
// Allows access to HTTP method middlewares that makes it easier to create routes for our application
const router = express.Router();
const taskController = require("../controllers/taskControllers.js");

// [SECTION] Routes

router.get("/", (req, res) => {

	taskController.getAllTasks().then(resultFromController => {res.send(resultFromController)});
});

router.post("/create", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => {res.send(resultFromController)});
});

router.delete("/:id", (req, res) => {
	
	taskController.deleteTask(req.params.id).then(resultFromController => {res.send(resultFromController)});
});

router.put("/:id", (req, res) => {

	taskController.updateTask(req.params.id, req.body).then(resultFromController => {res.send(resultFromController)});
})

// S36 Activity
router.get("/:id", (req, res) => {

	taskController.specificTask(req.params.id).then(resultFromController => {res.send(resultFromController)});
})

router.put("/:id/completed", (req, res) => {

	taskController.changeTaskStatus(req.params.id, req.body).then(resultFromController => {res.send(resultFromController)});
})



module.exports = router;